package flightweather.domain.chunkprocessingreport

case class ChunkProcessingReport(
    successCount: Int,
    errorCount: Int
) //value object
