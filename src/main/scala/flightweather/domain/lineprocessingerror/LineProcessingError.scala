package flightweather.domain.lineprocessingerror

import cats.data.*
import cats.data.Validated.*
import cats.implicits.*

import flightweather.domain.*

object LineProcessingError extends ValidatableEntity:
  type E = Entity
  type V = Validator

  case class Entity(
      header: EntityHeader,
      streamProcessingReportId: Option[Int] = None,
      lineNumber: Int,
      line: String,
      error: String
  ) extends EntityTrait(header)

  given defaultValidator: Validator = Validator()

  class Validator(minLineNumber: Int = 1) extends ValidatorTrait:
    def quickValidation(
        lineProcessingError: Entity
    ): Either[ValidationError, Entity] = for {
      _ <- validateLineNumber(lineProcessingError.lineNumber)
    } yield lineProcessingError

    def fullValidation(
        lineProcessingError: Entity
    ): ValidatedNec[ValidationError, Entity] =
      (
        lineProcessingError.header.validNec,
        lineProcessingError.streamProcessingReportId.validNec,
        validateLineNumber(lineProcessingError.lineNumber).toValidated
          .fold(err => err.invalidNec, lineNumber => lineNumber.validNec),
        lineProcessingError.line.validNec,
        lineProcessingError.error.validNec
      ).mapN(Entity.apply)

    def validateLineNumber(lineNumber: Int): Either[ValidationError, Int] = {
      if (lineNumber < minLineNumber) {
        Left(
          ValidationError(
            ValidationErrorType.InvalidLineNumber,
            "line number cannot be less than 1"
          )
        )
      } else {
        Right(lineNumber)
      }
    }
