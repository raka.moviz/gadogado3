package flightweather.domain

import java.time.ZonedDateTime
import cats.data.*
import cats.data.Validated.*
import cats.implicits.*

//https://stackoverflow.com/a/70324781
sealed abstract class ValidationErrorType
object ValidationErrorType {
  case object UnsupportedFileFormat extends ValidationErrorType
  case object UnsupportedProvider extends ValidationErrorType
  case object InvalidLineNumber extends ValidationErrorType
}

case class ValidationError(errorType: ValidationErrorType, message: String)

sealed case class EntityHeader(
    val id: Option[Int] = None,
    val createdAt: Option[ZonedDateTime] = None,
    val updatedAt: Option[ZonedDateTime] = None,
    val deletedAt: Option[ZonedDateTime] = None
)

trait ValidatableEntity:
  type E <: EntityTrait
  type V <: ValidatorTrait

  abstract class EntityTrait(header: EntityHeader)

  trait ValidatorTrait {
    def quickValidation(entity: E): Either[ValidationError, E]
    def fullValidation(entity: E): ValidatedNec[ValidationError, E]
  }

  extension (entity: E) {
    def quickValidation()(using validator: V) =
      validator.quickValidation(entity)
    def fullValidation()(using validator: V) = validator.fullValidation(entity)
  }

  given defaultValidator: V
