package flightweather.domain.streamprocessingreport

import cats.data.*
import cats.data.Validated.*
import cats.implicits.*

import flightweather.domain.*

enum StreamProcessingStatus(name: String):
  case Undefined extends StreamProcessingStatus("Undefined")
  case Running extends StreamProcessingStatus("Running")
  case Success extends StreamProcessingStatus("Success")
  case Failed extends StreamProcessingStatus("Failed")
  case Partial extends StreamProcessingStatus("Partial")

object StreamProcessingReport extends ValidatableEntity:
  type E = Entity
  type V = Validator

  case class Entity(
      header: EntityHeader,
      successCount: Int,
      errorCount: Int,
      reference: String,
      provider: String,
      format: String,
      path: String,
      status: StreamProcessingStatus,
      error: Option[String]
  ) extends EntityTrait(header)

  given defaultValidator: Validator = Validator(Set("wav"), Set("mrevil"))

  class Validator(supportedFormats: Set[String], approvedProviders: Set[String])
      extends ValidatorTrait:
    def quickValidation(
        streamProcessingReport: Entity
    ): Either[ValidationError, Entity] = for {
      _ <- validateFormat(streamProcessingReport.format)
      _ <- validateProvider(streamProcessingReport.provider)
    } yield streamProcessingReport

    def fullValidation(
        streamProcessingReport: Entity
    ): ValidatedNec[ValidationError, Entity] =
      (
        streamProcessingReport.header.validNec,
        streamProcessingReport.successCount.validNec,
        streamProcessingReport.errorCount.validNec,
        streamProcessingReport.reference.validNec,
        validateProvider(streamProcessingReport.provider).toValidated
          .fold(err => err.invalidNec, provider => provider.validNec),
        validateFormat(streamProcessingReport.format).toValidated
          .fold(err => err.invalidNec, format => format.validNec),
        streamProcessingReport.path.validNec,
        streamProcessingReport.status.validNec,
        streamProcessingReport.error.validNec
      ).mapN(Entity.apply)

    def validateFormat(format: String): Either[ValidationError, String] = {
      format match {
        case f if supportedFormats.contains(format) =>
          Right(format)
        case _ =>
          Left(
            ValidationError(
              ValidationErrorType.UnsupportedFileFormat,
              s"Format ${format} is not supported"
            )
          )
      }
    }

    def validateProvider(provider: String): Either[ValidationError, String] = {
      provider match {
        case p if approvedProviders.contains(provider) =>
          Right(provider)
        case _ =>
          Left(
            ValidationError(
              ValidationErrorType.UnsupportedProvider,
              s"Provider ${provider} is not approved"
            )
          )
      }
    }
