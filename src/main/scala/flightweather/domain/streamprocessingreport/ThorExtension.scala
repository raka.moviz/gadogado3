package flightweather.domain.streamprocessingreport

object ThorExtension {
  extension (streamProcessingReport: StreamProcessingReport.Entity) {
    def doThorThing(): Unit = {
      println("I do thor thing")
    }
  }
}
