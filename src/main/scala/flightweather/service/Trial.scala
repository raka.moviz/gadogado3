package flightweather.service

import zio.*
import flightweather.domain.*
import flightweather.domain.streamprocessingreport.*
import flightweather.domain.lineprocessingerror.*

trait TrialService {
  def doIt(): ZIO[Any, Nothing, Int]
}

object TrialServiceLive {
  val layer: ZLayer[Any, Nothing, TrialServiceLive] = ZLayer.succeed(TrialServiceLive())
}

class TrialServiceLive()(using
    streamProcessingReportValidator: StreamProcessingReport.Validator
)(using lineProcessingErrorValidator: LineProcessingError.Validator) extends TrialService  {
  override def doIt(): ZIO[Any, Nothing, Int] = {
    val streamProcessingReport = StreamProcessingReport.Entity(
      header = EntityHeader(),
      successCount = 1,
      errorCount = 1,
      reference = "ref",
      provider = "provi1",
      format = "fmt1",
      path = "thepath",
      status = StreamProcessingStatus.Running,
      error = None
    )

    import ThorExtension._
    streamProcessingReport.doThorThing()

    val quickValidationResult = streamProcessingReport.quickValidation()
    println(quickValidationResult)

    val fullValidationResult = streamProcessingReport.fullValidation()
    println(fullValidationResult)

    val streamProcessingReport2 =
      streamProcessingReport.copy(format = "wav", provider = "mrevil")

    val quickValidationResult2 = streamProcessingReport2.quickValidation()
    println(quickValidationResult2)

    val fullValidationResult2 = streamProcessingReport2.fullValidation()
    println(fullValidationResult2)

    val lineProcessingError = LineProcessingError.Entity(
      header = EntityHeader(),
      streamProcessingReportId = None,
      lineNumber = 1,
      line = "hello",
      error = "error1"
    )

    val quickValidationResult3 = lineProcessingError.quickValidation()
    println(quickValidationResult3)

    val fullValidationResult3 = lineProcessingError.fullValidation()
    println(fullValidationResult3)

    val lineProcessingError2 = lineProcessingError.copy(lineNumber = 0)

    val quickValidationResult4 = lineProcessingError2.quickValidation()
    println(quickValidationResult4)

    val fullValidationResult4 = lineProcessingError2.fullValidation()
    println(fullValidationResult4)

    ZIO.succeed(4)
  }
}
