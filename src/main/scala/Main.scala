import flightweather.domain.streamprocessingreport.*
import flightweather.domain.lineprocessingerror.*
import flightweather.service.*
import zio.*

val mijnApp: ZIO[TrialService, Nothing, Int] =
  for {
    t <- ZIO.service[TrialService]
    x <- t.doIt()
  } yield (x)

object MijnApp extends ZIOAppDefault {
  def run = mijnApp.provide(
    TrialServiceLive.layer
  )
}