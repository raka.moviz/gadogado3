val scala3Version = "3.2.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "gadogado3",
    version := "0.1.0-SNAPSHOT",

    scalaVersion := scala3Version,

    libraryDependencies ++= Seq(
      "org.scalameta" %% "munit" % "0.7.29" % Test,
      "org.scalaz" %% "scalaz-core" % "7.3.6",
      "org.typelevel" %% "cats-core" % "2.8.0",
      "dev.zio" %% "zio" % "2.0.0" 
    )
  )
